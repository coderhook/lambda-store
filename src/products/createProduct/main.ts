import {APIGatewayProxyResult} from 'aws-lambda'
import {v4 as uuid} from 'uuid'
import * as AWS from 'aws-sdk'
import {commonMiddleware} from '../../utils/commonMiddleware'

import createHttpError from 'http-errors'
import {Product} from '../interfaces'

const dynamodb = new AWS.DynamoDB.DocumentClient()

type Event = {
  body: Product
}

const createProduct = async (event: Event): Promise<APIGatewayProxyResult> => {
  const {
    model,
    name,
    description,
    category,
    images,
    price,
    inventory,
  } = event.body

  const now = new Date()

  const product: Product = {
    id: uuid(),
    createdAt: now.toISOString(),
    model,
    name,
    description,
    category,
    images,
    price,
    inventory,
  }

  try {
    await dynamodb
      .put({
        TableName: process.env.PRODUCTS_TABLE_NAME || 'ProductsTable-dev',
        Item: product,
      })
      .promise()
  } catch (e) {
    throw new createHttpError.InternalServerError(e)
  }

  return {
    statusCode: 201,
    body: JSON.stringify(product),
  }
}

export const handler = commonMiddleware(createProduct)
