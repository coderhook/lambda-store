import * as AWS from 'aws-sdk'
import {commonMiddleware} from '../../utils/commonMiddleware'
import createHttpError from 'http-errors'
import cors from '@middy/http-cors'
import {APIGatewayProxyResult} from 'aws-lambda'

const dynamodb = new AWS.DynamoDB.DocumentClient()

const getProducts = async (): Promise<APIGatewayProxyResult> => {
  let products
  try {
    products = await dynamodb
      .scan({
        TableName: process.env.PRODUCTS_TABLE_NAME || 'productsTable-dev',
      })
      .promise()
  } catch (e) {
    throw new createHttpError.InternalServerError(e)
  }

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({data: products.Items}),
  }
}

export const handler = commonMiddleware(getProducts).use(cors())
