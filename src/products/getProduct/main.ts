import {APIGatewayProxyEvent, APIGatewayProxyResult} from 'aws-lambda'
import * as AWS from 'aws-sdk'
import createHttpError from 'http-errors'
import {commonMiddleware} from '../../utils/commonMiddleware'

const dynamodb = new AWS.DynamoDB.DocumentClient()

const getProduct = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  const id = event.pathParameters?.id
  let product

  try {
    const result = await dynamodb
      .get({
        TableName: process.env.PRODUCTS_TABLE_NAME || 'productsTable-dev',
        Key: {id},
      })
      .promise()

    product = result.Item
  } catch (e) {
    throw new createHttpError.InternalServerError(e)
  }

  if (!product)
    throw new createHttpError.NotFound(`Product with id: "${id}" not found`)

  return {
    statusCode: 200,
    body: JSON.stringify(product),
  }
}

export const handler = commonMiddleware(getProduct)
