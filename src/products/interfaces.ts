export interface Product {
  id: string
  createdAt: string
  model: string
  name: string
  description: string
  category: string
  images: string[]
  price: {
    unit_amount: number
    currency: string
  }
  inventory: {
    p35: number
    p36: number
    p37: number
  }
  background_colour?: string | null
}

export interface StripeProduct {
  id: string
  name: string
  description: string
  images: string[]
  type: string
  metadata: {
    category: string
    model: string
    p35: number
    p36: number
    p37: number
  }
}
