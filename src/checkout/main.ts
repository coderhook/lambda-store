import {APIGatewayProxyEvent, APIGatewayProxyResult, Context} from 'aws-lambda'

async function checkout(
  event: APIGatewayProxyEvent,
  context: Context,
): Promise<APIGatewayProxyResult> {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'This is the checkout lambda function',
      event,
      context,
    }),
  }
}

export const handler = checkout
