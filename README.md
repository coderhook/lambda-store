# Lambda Serverless ShoeStore

## Service Information

Service Information service: lambda-shoe-store stage: dev region: eu-west-1
stack: lambda-shoe-store-dev resources: 30 api keys: None endpoints: GET -
https://ttgp4p115h.execute-api.eu-west-1.amazonaws.com/dev/checkout POST -
https://ttgp4p115h.execute-api.eu-west-1.amazonaws.com/dev/products GET -
https://ttgp4p115h.execute-api.eu-west-1.amazonaws.com/dev/products GET -
https://ttgp4p115h.execute-api.eu-west-1.amazonaws.com/dev/product/{id}
functions: checkout: lambda-shoe-store-dev-checkout createProduct:
lambda-shoe-store-dev-createProduct getProducts:
lambda-shoe-store-dev-getProducts getProduct: lambda-shoe-store-dev-getProduct
layers: None

# Base Serverless Framework Template

## What's included

- Folder structure used consistently across our projects.
- [serverless-pseudo-parameters plugin](https://www.npmjs.com/package/serverless-pseudo-parameters):
  Allows you to take advantage of CloudFormation Pseudo Parameters.
- [serverless-bundle plugin](https://www.npmjs.com/package/serverless-pseudo-parameters):
  Bundler based on the serverless-webpack plugin - requires zero configuration
  and fully compatible with ES6/ES7 features.
